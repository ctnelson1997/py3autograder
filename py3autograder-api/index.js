const express = require('express');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const _ = require('lodash');
const crypto = require('crypto')
const schedule = require('node-schedule')
const { spawn } = require("child_process");
const fs = require('fs');
const path = require('path');
const { result } = require('lodash');

const app = express();

let activeWorkers = 0
const workerQueue = []

const NUM_WORKERS = 4

const STATUS_WAITING = "WAITING"
const STATUS_PENDING = "PENDING"
const STATUS_DONE = "DONE"

const JOBS = new Map();


// enable files upload
app.use(fileUpload({
    createParentPath: true
}));

//add other middleware
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('dev'));

//start app 
const port = 4001;

app.get('/health', (req, res) => {
    res.status(200).send({
        queueLength: workerQueue.length,
        activeWorkers: activeWorkers
    })
})

app.get('/status/:uuid', function (req, res) {
    const uuid = req.params.uuid;
    if(JOBS.has(uuid)) {
        let status = JOBS.get(uuid);
        let queueIndex = -1;
        if(status == STATUS_WAITING) {
            queueIndex = workerQueue.indexOf(uuid);
        }
        if(status == STATUS_DONE) {
            console.log("yes tho")
            fs.readFile(path.join(__dirname, 'uploads', uuid, 'result.txt'), 'utf8' , (err, fileData) => {
                if (err) {
                  console.error(err)
                }
                res.status(200).send({
                    status: status,
                    queueIndex: queueIndex,
                    result: fileData
                })
              })
            
        } else {
            res.status(200).send({
                status: status,
                queueIndex: queueIndex
            })
        }
        
    } else {
        res.status(404).send({
            msg: "No job with given uuid"
        })
    }
});

app.post('/uploadFile', async (req, res) => {
    try {
        if (!req.files) {
            res.send({
                status: false,
                message: 'No file uploaded'
            });
        } else {

            let notebook = req.files.file;

            const uuid = new Date().getTime() + "_" + crypto.randomUUID().replace(/-/g, "_");
            console.log(uuid)

            const nbName = "main.ipynb";
            fs.mkdir(path.join(__dirname, 'uploads', uuid), (err) => {
                if (err) {
                    console.error(err);
                }
                notebook.mv(path.join(__dirname, 'uploads', uuid, nbName));
                //send response
                res.send({
                    message: 'File uploaded successfully.',
                    name: nbName,
                    mimetype: notebook.mimetype,
                    size: notebook.size,
                    hash: crypto.createHash('sha256').update(notebook.data).digest('hex'),
                    uuid: uuid
                });
                JOBS.set(uuid, STATUS_WAITING)
                scheduleWorker(uuid);
            });

        }
    } catch (err) {
        console.log(err)
        res.status(500).send(err);
    }
});

app.listen(port, () =>
    console.log(`App is listening on port ${port}.`)
);

function scheduleWorker(uuid) {
    workerQueue.push(uuid);
}

function runWorker(uuid) {
    let output = ""
    const workerProcess = spawn('docker', ['run',
        '--volume',
        `/c/Users/ColeNelson/Desktop/CS220/py3autograder/py3autograder-api/uploads/${uuid}:/submission`,
        'ctnelson1997/py3autograder:latest']);
    workerProcess.stdout.on('data', (data) => {
        output += data + '\n'
    });

    workerProcess.stderr.on('data', (data) => {
        output += data + '\n'
    });

    workerProcess.on('close', (code) => {
        fs.writeFile(path.join(__dirname, 'uploads', uuid, 'result.txt'), output, (err) => {
            if (err) {
                console.error(err);
            }
            activeWorkers -= 1
            JOBS.set(uuid, STATUS_DONE);
            console.log(`child process exited with code ${code}`);
        })

    });

}

schedule.scheduleJob('* * * * * *', function () {
    console.log("Active Workers:" + activeWorkers);
    console.log("Queued Jobs:" + workerQueue.length);
    while (activeWorkers < NUM_WORKERS && workerQueue.length != 0) {
        activeWorkers += 1
        const nextUuid = workerQueue.pop();
        JOBS.set(nextUuid, STATUS_PENDING);
        runWorker(nextUuid);
    }
});