# Py3 Autograder

Autograder for Python 3 to run student submissions in isolated environments.

Available on Dockerhub as [ctnelson1997/py3autograder](https://hub.docker.com/r/ctnelson1997/py3autograder) (currently built as P2 from F21).

## Build

Place the program files in`/build/includes/`. This includes the `test.py` and whatever data/auxiliary files are associated with the program (for P2, this was only the `test.py` file). Run `/build/build.bat`.

In the future, we can version programs such as `ctnelson1997/py3autograder:p3`

## Run

Set the absolute submission directory (where the `main.ipynb` will be dropped off) as the `DROPOFF_DIR` in `/execution/run.bat` and run the batch file.

