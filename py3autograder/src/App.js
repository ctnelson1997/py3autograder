import React, { useState, useRef } from 'react';
import ProgressBar from 'react-bootstrap/ProgressBar'
import Dropdown from 'react-bootstrap/Dropdown'
import Button from 'react-bootstrap/Button';
import './App.css';

import axios from 'axios';

function App() {

  const [programs, setPrograms] = useState([
    "P1", "P2", "P3", "P4", "P5"
  ]);

  const STATUS_WAITING = "WAITING"
  const STATUS_PENDING = "PENDING"
  const STATUS_DONE = "DONE"

  const STATE_NA = {
    percent: 0,
    msg: "Waiting"
  }

  const STATE_READY = {
    percent: 20,
    msg: "Ready!"
  }

  const STATE_QUEUED = {
    percent: 40,
    msg: "Queued..."
  }

  const STATE_GRADING = {
    percent: 70,
    msg: "Grading..."
  }

  const STATE_DONE = {
    percent: 100,
    msg: "Done!"
  }

  const [currentProgram, setCurrentProgram] = useState(undefined);
  const [uploadedFile, setUploadedFile] = useState(undefined);
  const [gradeProgress, setGradeProgress] = useState(STATE_NA);
  const [queueIndex, setQueueIndex] = useState(0);
  const [graderOutput, setGraderOutput] = useState(undefined);

  const inputRef = useRef(null);

  const handleUpload = () => {
    inputRef.current?.click();
  };
  const handleDisplayFileDetails = () => {
    inputRef.current?.files &&
      setUploadedFile({
        content: inputRef.current.files[0],
        datetime: new Date().getTime()
      });
      setGradeProgress(STATE_READY);
  };

  function handleSubmit() {
    const url = 'http://localhost:4001/uploadFile';
    const formData = new FormData();
    formData.append('file', uploadedFile.content);
    formData.append('fileName', uploadedFile.content.name);
    const config = {
      headers: {
        'content-type': 'multipart/form-data',
      },
    };
    axios.post(url, formData, config).then((response) => {
      setGradeProgress(STATE_QUEUED);
      
      const uuid = response.data.uuid;
      console.log(response.data)
      console.log(uuid)
      let timerId = setInterval((uuid) => {
        axios.get("http://localhost:4001/status/" + uuid).then((response) => {
          const status = response.data.status;
          const respQueueIndex = response.data.queueIndex;
          if(status == STATUS_WAITING) {
            setQueueIndex(respQueueIndex + 1);
            setGradeProgress(STATE_QUEUED);
          } else if (status == STATUS_PENDING) {
            setGradeProgress(STATE_GRADING);
          } else if (status == STATUS_DONE) {
            setGradeProgress(STATE_DONE);
            setGraderOutput(response.data.result)
            clearInterval(timerId)
          }
          
        });
      }, 1000, uuid);
    });
  }


  return (
    <div className="App">
      <header className="App-header">

        <div>
          <h1>CS220 Py3 Tester</h1>
          <p>This is an experimental tester for CS220 meant to get you rapid results on your submission. </p>
          <p>This does <b>not</b> substitute for your project submission.</p>
          <p>Results are <b>not</b> guaranteed.</p>
          <p>Please complete your submission <a href="https://www.msyamkumar.com/cs220/s22/submission.html">on the course website.</a></p>
        </div>

        <div style={{ display: "inline-block" }}>
          <Dropdown className="d-inline mx-2">
            <Dropdown.Toggle variant="secondary" id="dropdown-basic" style={{ align: "right" }}>
              {currentProgram ? currentProgram : "Choose Your Program"}
            </Dropdown.Toggle>

            <Dropdown.Menu>
              {genPrograms(programs, setCurrentProgram)}
            </Dropdown.Menu>
          </Dropdown>

          <input
            ref={inputRef}
            onChange={handleDisplayFileDetails}
            className="d-none"
            type="file"
            accept=".ipynb"
          />
          <Button variant="light" onClick={handleUpload} className={`d-inline mx-2`} variant={`${uploadedFile ? "success" : "light"}`}>{uploadedFile ? uploadedFile.content.name : "Upload Your Notebook"}</Button>{' '}

          <Button variant="primary" onClick={handleSubmit} className="d-inline mx-2">Run Tests</Button>{' '}
          
        </div>
        <div style={{width: "50%"}}>
        <ProgressBar style={{marginTop: "1rem"}}animated now={gradeProgress.percent} label={gradeProgress.msg} />
        </div>
        {uploadedFile ? <p style={{marginTop: "1rem"}}>{"Notebook Uploaded: " + new Date(uploadedFile.datetime).toLocaleString()}</p> : <></>}
        {
          graderOutput ?
          <div className="input-group" style={{width: "50%", marginTop: "1rem"}}>
            <textarea className="form-control" id="exampleFormControlTextarea1" rows="20" readOnly style={{fontSize: "0.75rem"}}>{graderOutput}</textarea>
          </div>
          :
          <></>
        }
        
        
      </header>
    </div>
  );
}

function genPrograms(programs, setCurrentProgram) {
  let progs = [];
  programs.forEach(program => progs.push(genProgram(program, setCurrentProgram)))
  return progs;
}

function genProgram(prog, setCurrentProgram) {
  return <Dropdown.Item onClick={() => {
    setCurrentProgram(prog)
  }} key={prog}>{prog}</Dropdown.Item>
}


export default App;
